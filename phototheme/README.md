# phototheme theme

forked from hubzilla's default redbasic theme.

trying to create a theme that's suitable for photo-centered streams.

to install it go to the hub's webroot and issue the following command:

<code>util/add_theme_repo git@framagit.org:phani00/phototheme.git phototheme</code>
